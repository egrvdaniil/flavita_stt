from django import template
import datetime
import transliterate
import django.utils.text

register = template.Library()


@register.filter
def to_time(time_int):
    res = datetime.timedelta(seconds=round(time_int))
    return str(res)


@register.filter
def translit_slugify(text):
    res = transliterate.slugify(text)
    if res is None:
        return django.utils.text.slugify(text)
    return res
