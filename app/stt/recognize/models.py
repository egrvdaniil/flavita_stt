from django.db import models
import os
import os.path
import transliterate
import django.utils.text


def get_audio_file_path(instance, filename):
    head, tail = os.path.splitext(os.path.basename(filename))
    head_slug = django.utils.text.slugify(head) if (slug := transliterate.slugify(head)) is None else slug
    return os.path.join("audio", head_slug[:20]+tail)


class AudioFile(models.Model):
    name = models.CharField(max_length=200, help_text="Name of the audio")
    file = models.FileField(upload_to=get_audio_file_path)
    mime_type = models.CharField(max_length=200)


class Recognize(models.Model):
    class Statuses(models.TextChoices):
        QUEUED = "в очереди"
        STARTED = "обрабатывается"
        FINISHED = "завершен"
        ERROR = "ошибка"
    audio = models.OneToOneField(AudioFile, on_delete=models.CASCADE, primary_key=True)
    status = models.CharField(choices=Statuses.choices, max_length=20)
    task_id = models.CharField(null=True, unique=True, max_length=200)
    result_file = models.FileField(upload_to="results/", null=True)
    creation_time = models.DateTimeField(auto_now_add=True)
    finish_time = models.DateTimeField(null=True)
    message = models.TextField(null=True)
