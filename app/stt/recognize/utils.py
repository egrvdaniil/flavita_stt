import datetime


def format_result(results) -> str:
    out = ""
    for i, part in enumerate(results):
        if i == 0:
            out += f"--- PART 1 ---\n\n"
        else:
            out += f"\n--- PART {i+1} ---\n\n"
        for el in part:
            out += f"спикер-{el['speaker']} ({str(datetime.timedelta(seconds=round(el['part_start'])))}-{str(datetime.timedelta(seconds=round(el['part_end'])))}): {el['text']}\n\n"
    return out
