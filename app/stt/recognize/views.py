from django.shortcuts import render, redirect, get_object_or_404
from .utils import format_result
import json
from django.http import HttpRequest
from .forms import UploadAudio
from .models import Recognize, AudioFile
from .task import start_recognize
from celery import uuid
import magic
from celery.result import AsyncResult
from django.conf import settings
from stt.celery_init import debug_task
from django.http import HttpResponse


# Create your views here.
def index(request: HttpRequest):
    return render(request, "stt/index.html")

def get_tasks(request: HttpRequest):
    unfinished_exist = Recognize.objects.exclude(status__in=["завершен", "ошибка"]).exists()
    audios = AudioFile.objects.order_by("-id").all()
    return render(request, "stt/ajax/task_list.html", {"audios": audios, "unfinished_exist": unfinished_exist})

def load_audio(request: HttpRequest):
    if request.method == "POST":
        form = UploadAudio(request.POST, request.FILES)
        if form.is_valid():
            audio: AudioFile = form.save(commit=False)
            audio.mime_type = magic.from_buffer(audio.file.read(2048), mime=True)
            audio.file.seek(2048)
            audio.save()
            task_id = uuid()
            rec = Recognize(
                audio=audio,
                status=Recognize.Statuses.QUEUED,
                task_id=task_id,
            )
            rec.save()
            start_recognize.apply_async((audio.id,), task_id=task_id)
            return redirect("index")
    form = UploadAudio()
    return render(request, "stt/load_audio.html", {"form": form})


def recognize_detail(request: HttpRequest, audio_id: int):
    audio = get_object_or_404(AudioFile, id=audio_id)
    with audio.recognize.result_file.open() as f:
        recognize_results = json.load(f)
        raw_text = format_result(recognize_results)
    context = {
        "audio": audio,
        "recognize_results": recognize_results,
        "raw_text": raw_text,
    }
    return render(request, "stt/recognize_detail.html", context)


def delete_recognize(request: HttpRequest, audio_id: int):
    audio = get_object_or_404(AudioFile, id=audio_id)
    res = AsyncResult(audio.recognize.task_id)
    res.forget()
    audio.delete()
    return HttpResponse()
