from django.urls import path
from .views import index, load_audio, recognize_detail, get_tasks, delete_recognize
from django.contrib.auth import views as auth_views
from django.conf import settings
from django.conf.urls.static import static


urlpatterns = [
    path("", index, name="index"),
    path("get_tasks/", get_tasks, name="get_tasks"),
    path("login/", auth_views.LoginView.as_view(template_name="stt/login.html"), name="login"),
    path("logout/", auth_views.LogoutView.as_view(), name="logout"),
    path("upload/", load_audio, name="upload"),
    path("<int:audio_id>/", recognize_detail, name="recognize_detail"),
    path("<int:audio_id>/delete", delete_recognize, name="delete_recognize"),
]

if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
