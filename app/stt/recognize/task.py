from celery import shared_task
from .audio_processing import Transcription
from .models import Recognize, AudioFile
from django.core.files.base import ContentFile
from django.utils import timezone
from django.conf import settings
import json


@shared_task
def start_recognize(audio_id):
    audio = AudioFile.objects.get(pk=audio_id)
    rec = audio.recognize
    rec.status = Recognize.Statuses.STARTED
    rec.save()
    try:
        transcription = Transcription(settings.STT_LOGIN, int(settings.STT_DOMAIN), settings.STT_PASSWORD)
        transcription.from_file(audio.file.path)
        results = transcription.start()
    except Exception as exc:
        rec.status = Recognize.Statuses.ERROR
        print(exc)
        rec.message = str(exc)
        rec.save()
        return
    rec.status = Recognize.Statuses.FINISHED
    result_file = ContentFile(json.dumps(results))
    rec.finish_time = timezone.now()
    rec.result_file.save(f"{rec.audio.id}.json", result_file)
