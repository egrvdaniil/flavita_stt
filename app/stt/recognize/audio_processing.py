import json
from cloud_client import DiarizationApi, AudioDto, AuthRequestDto
from cloud_client.api.session_api import SessionApi
import base64
from pydub import AudioSegment
import io
import requests as rq


def log_info(*args, **kwargs):
    print(*args, **kwargs)


class Transcription:
    def __init__(self, login, domain, passw):
        session_api = SessionApi()
        credentials = AuthRequestDto(login, domain, passw)
        log_info("create session")
        self.session = session_api.login(credentials).session_id
        self.audio = None

    def from_file(self, filename):
        log_info("loading file")
        self.audio = AudioSegment.from_file(filename)
        log_info("file loaded")

    def start_part_diarization(self, audio_part):
        diarizationApi = DiarizationApi()
        audio_b = io.BytesIO()
        audio_part.export(audio_b)
        audio_b.seek(0)
        data = audio_b.read()
        encoded_string = base64.standard_b64encode(data)
        encoded_string = str(encoded_string, 'ascii', 'ignore')
        diarization_param = AudioDto(encoded_string)
        log_info("start diarization")
        diarization_result = diarizationApi.diarization(self.session, diarization_param)
        return diarization_result.to_dict()

    @staticmethod
    def format_diarization_part_result(result, part_end):
        log_info(f"start formation")
        parts_raw = []
        for speaker in result["speakers"]:
            for segment in speaker["segments"]:
                parts_raw.append((segment["start"], segment["length"], speaker["number"]))

        def custom_key(k):
            return k[0]

        parts_raw.sort(key=custom_key)

        parts = []
        p_st = parts_raw[0][0]
        p_spe = parts_raw[0][2]
        for part in parts_raw[1:]:
            if p_spe != part[2]:
                parts.append((p_st, part[0], p_spe))
                p_st = part[0]
                p_spe = part[2]
            else:
                if part[0] - p_st >= 60:
                    parts.append((p_st, part[0], p_spe))
                    p_st = part[0]
        parts.append((p_st, part_end, p_spe))
        return parts

    def recognize(self, diarization_result, ppart, part_start=0):
        log_info(f"start recognize {part_start}")
        results = []
        for part in diarization_result:
            audio_part_b = io.BytesIO()
            audio_part = ppart[round(part[0] * 1000):round(part[1] * 1000)]
            audio_part = audio_part.set_channels(1)
            audio_part.export(audio_part_b, format="wav")
            audio_part_b.seek(0)
            audio_part_base64 = base64.b64encode(audio_part_b.read()).decode('utf-8')
            audio_json = {
                "model_id": "FarFieldRus10:offline",
                "audio": {
                    "data": audio_part_base64,
                    "mime": "WAV"
                }
            }
            res = rq.put("https://cloud.speechpro.com/vkasr/rest/v2/recognizer/simple",
                         headers={"X-Session-Id": self.session}, json=audio_json)
            if res.status_code == 400 and res.json()["message"] == "Speech not found":
                continue
            print(res.json())
            result = {
                "text": res.json()["text"],
                "part_start": part[0] + part_start,
                "part_end": part[1] + part_start,
                "speaker": part[2],
            }
            results.append(result)
        return results

    def start(self, part_size=20):
        duration = len(self.audio)
        part_size_mil = part_size * 60 * 1000
        parts = []
        for i in range(0, duration, part_size * 60 * 1000):
            log_info(f"start diarization {i} - {i + part_size_mil}")
            if i + part_size_mil < duration:
                audio_part = self.audio[i:i + part_size_mil]
                part_end = part_size_mil // 1000
            else:
                audio_part = self.audio[i:]
                part_end = (duration - i) // 1000
            diarization_result = self.start_part_diarization(audio_part)
            formated_diarization_result = self.format_diarization_part_result(diarization_result, part_end)
            parts.append(self.recognize(formated_diarization_result, audio_part, part_start=i // 1000))
        return parts




