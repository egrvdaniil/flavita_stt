from django import forms
from .models import AudioFile


class UploadAudio(forms.ModelForm):
    class Meta:
        model = AudioFile
        fields = ("name", "file")
