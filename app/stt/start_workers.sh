#!/bin/sh

set -e

. /venv/bin/activate

exec celery -A stt worker --loglevel=info --concurrency=1